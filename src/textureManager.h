#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include <map>
#include <string>

#ifndef __APPLE__
 #include <GL/gl.h>
#else
 #include <OpenGL/gl.h>
#endif

class TextureManager
{
  public:
    TextureManager();
    ~TextureManager();
    void add(const std::string &name,
             const void *data,
             GLsizei size);
    void use(const std::string &name);

  private:
    std::map<std::string, GLuint> _map;
};

#endif
