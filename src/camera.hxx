inline void
Camera::set_pos(const Point &pos)
{
  _pos = pos;
}

inline void
Camera::set_target(const Point &target)
{
  _target = target;
}

inline Point
Camera::pos() const
{
  return _pos;
}

inline Point
Camera::target() const
{
  return _target;
}

inline void
Camera::set_x(const float x)
{
  _pos.set_x(x);
}

inline void
Camera::set_y(const float y)
{
  _pos.set_y(y);
}

inline void
Camera::set_z(const float z)
{
  _pos.set_z(z);
}

inline void
Camera::set_roll(const float roll)
{
  _roll = roll;
}

inline float
Camera::x() const
{
  return _pos.x();
}

inline float
Camera::y() const
{
  return _pos.y();
}

inline float
Camera::z() const
{
  return _pos.z();
}
