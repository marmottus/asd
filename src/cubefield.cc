#include <cstdlib>
#include "cubefield.h"
#include <iostream>
#include "draw.h"
#include <iostream>
#include <cmath>

/*
 * Chunk
 */

Chunk::Chunk(const Point &center)
  : _center(center),
    _cubes (std::list<Cube>()),
    _dl(glGenLists(1)),
    _visible(true)
{
  for (int i = - NB_CUBES; i < NB_CUBES; i++)
    for (int j = - NB_CUBES; j < NB_CUBES; j++)
    {
      Point p(center.x() + i * SPACING,
              center.y() + CUBE_SIZE/2.0,
              center.z() + j * SPACING);
      p.addRandomDisplacement();
      _cubes.push_back(Cube(p));
    }

  Color c(0.5, 0.5, 0.5);
  std::list<Cube>::iterator it;
  for (it = _cubes.begin(); it != _cubes.end(); it++)
  {
    c = Color(c.r() + (rand()%100 - 50)/800.0,
              c.g() + (rand()%100 - 50)/800.0,
              c.b() + (rand()%100 - 50)/800.0);
    it->set_color(c);
    it->draw();
  }
  glNewList(_dl, GL_COMPILE);
  for (it = _cubes.begin(); it != _cubes.end(); it++)
    it->draw();
  glEndList();
}

Chunk::~Chunk()
{
  glDeleteLists(_dl, 1);
  _cubes.clear();
}

void Chunk::draw() const
{
  if (_visible)
    glCallList(_dl);
}

Cubefield::Cubefield()
  : _chunks(std::list<Chunk*>())
{
  _chunks.push_back(new Chunk(Point(0,0,0)));
}

Cubefield::~Cubefield()
{
  for (auto chunk : _chunks)
    delete chunk;
  _chunks.clear();
}

void Cubefield::draw() const
{
  for (auto chunk : _chunks)
    chunk->draw();
}

void Cubefield::update(const Point &ship_pos)
{
  // Hide chunk if it's too far away
  for (auto chunk : _chunks)
  {
    float m = 0; // manhattan distance
    m += fabs(chunk->center().x() - ship_pos.x());
    m += fabs(chunk->center().z() - ship_pos.z());
    chunk->setVisible(m <= 50);
  }

  // Create Chunk if needed
  float x = floor(ship_pos.x() / (2*NB_CUBES*SPACING*CUBE_SIZE+MAX_DIFF));
  x *= (2*NB_CUBES*SPACING*CUBE_SIZE+MAX_DIFF);
  x += (2*NB_CUBES*SPACING*CUBE_SIZE+MAX_DIFF) / 2.0;
  float z = floor(ship_pos.z() / (2*NB_CUBES*SPACING*CUBE_SIZE+MAX_DIFF));
  z *= (2*NB_CUBES*SPACING*CUBE_SIZE+MAX_DIFF);
  z += (2*NB_CUBES*SPACING*CUBE_SIZE+MAX_DIFF) / 2.0;

  for (auto chunk : _chunks)
    if (chunk->center().x() == x && chunk->center().z() == z)
      return;

  _chunks.push_back(new Chunk(Point(x, 0, z)));
}

/*
 * Ship
 */
Ship::Ship()
  : _mesh (0),
  _speed (Point(0, 0, 0))
{
  Point center(0 * SPACING, CUBE_SIZE/2.0, 0 * SPACING);
  _mesh = new Cube(center);
  _mesh->set_color(255, 255, 255);
}

Ship::~Ship ()
{
  delete _mesh;
}

void Ship::addSpeed(float x, float y, float z)
{
  _speed.set_x(_speed.x() + x);
  if (_speed.x() > MAX_SHIP_SPEED)
    _speed.set_x(MAX_SHIP_SPEED);
  if (_speed.x() < -MAX_SHIP_SPEED)
    _speed.set_x(-MAX_SHIP_SPEED);

  _speed.set_y(_speed.y() + y);
  if (_speed.y() > MAX_SHIP_SPEED)
    _speed.set_y(MAX_SHIP_SPEED);
  if (_speed.y() < -MAX_SHIP_SPEED)
    _speed.set_y(-MAX_SHIP_SPEED);

  _speed.set_z(_speed.z() + z);
  if (_speed.z() > MAX_SHIP_SPEED)
    _speed.set_z(MAX_SHIP_SPEED);
  if (_speed.z() < -MAX_SHIP_SPEED)
    _speed.set_z(-MAX_SHIP_SPEED);
}

void Ship::update(unsigned int etime)
{
  // Apply current speed
  _mesh->set_center(_mesh->center().x() + _speed.x() * etime/2.0,
                    _mesh->center().y() + _speed.y() * etime/2.0,
                    _mesh->center().z() + _speed.z() * etime/2.0);

  // Apply friction
  if (fabs(_speed.x()) > 0.00001)
    _speed.set_x(_speed.x()
        + (_speed.x() > 0
          ? -SHIP_FRICTION * etime/1000.0
          :  SHIP_FRICTION * etime/1000.0));

  if (fabs(_speed.y()) > 0.00001)
    _speed.set_y(_speed.y()
        + (_speed.y() > 0
          ? -SHIP_FRICTION * etime/1000.0
          :  SHIP_FRICTION * etime/1000.0));

  if (fabs(_speed.z()) > 0.00001)
    _speed.set_z(_speed.z()
        + (_speed.z() > 0
          ? -SHIP_FRICTION * etime/1000.0
          :  SHIP_FRICTION * etime/1000.0));
}

void Ship::draw() const
{
  glPushMatrix();
  _mesh->translate();
  draw_cube();
  glPopMatrix();
}

/*
 * Cube
 */

// Create a cube with random angle and color
Cube::Cube(const Point &center)
  : _size (CUBE_SIZE),
    _angle (rand() % 360),
    _center (center),
    _color (Color())
{
}

void Cube::draw() const
{
  glPushMatrix();
  _color.use();
  _center.translate();
  //glRotatef(_angle, 1, 1, 1);
  draw_cube();
  glPopMatrix();
}

void Cube::translate() const
{
  _color.use();
  _center.translate();
}

void Cube::set_color(float r, float g, float b)
{
  _color = Color(r, g, b);
}

void Cube::set_center(float x, float y, float z)
{
  _center.set_x(x);
  _center.set_y(y);
  _center.set_z(z);
}

/*
 * Color
 */
#define clamp(x) (x < 0.0 ? 0.0 : (x > 1.0 ? 1.0 : x))
Color::Color(float r, float g, float b)
  : _r (clamp(r)),
    _g (clamp(g)),
    _b (clamp(b))
{
}

Color::Color()
  : _r (rand() % 255 / 255.0),
    _g (rand() % 255 / 255.0),
    _b (rand() % 255 / 255.0)
{
}

void Color::use() const
{
  GLfloat c[4] = {_r, _g, _b, 1.0};
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, c);
}

void Color::use(float r, float g, float b)
{
  GLfloat c[4] = {r, g, b, 1.0};
  glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, c);
}

/*
 * Point
 */

  Point::Point(float x, float y, float z)
: _x(x), _y(y), _z(z)
{}

void Point::addRandomDisplacement()
{
  _x += rand() % MAX_DIFF;
  //_y += rand() % MAX_DIFF;
  _z += rand() % MAX_DIFF;
}

void Point::translate() const
{
  glTranslatef(_x, _y, _z);
}
