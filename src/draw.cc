#ifndef __APPLE__
 #include <GL/gl.h>
#else
 #include <OpenGL/gl.h>
#endif
#include "textureManager.h"

void draw_cube()
{
  const int tex_repeat = 4;
  glBegin(GL_QUADS);

  // Bottom
  glNormal3f(0, -1, 0);
  glTexCoord2f(0, 0);
  glVertex3f(-0.5, -0.5, -0.5);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(-0.5, -0.5, 0.5);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(0.5, -0.5, 0.5);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(0.5, -0.5, -0.5);

  // Top
  glNormal3f(0, 1, 0);
  glTexCoord2f(0, 0);
  glVertex3f(-0.5, 0.5, -0.5);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(0.5, 0.5, -0.5);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(0.5, 0.5, 0.5);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(-0.5, 0.5, 0.5);

  // back
  glNormal3f(0, 0, -1);
  glTexCoord2f(0, 0);
  glVertex3f(-0.5, -0.5, -0.5);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(0.5, -0.5, -0.5);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(0.5, 0.5, -0.5);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(-0.5, 0.5, -0.5);

  // front
  glNormal3f(0, 0, 1);
  glTexCoord2f(0, 0);
  glVertex3f(-0.5, -0.5, 0.5);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(-0.5, 0.5, 0.5);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(0.5, 0.5, 0.5);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(0.5, -0.5, 0.5);

  // left
  glNormal3f(-1, 0, 0);
  glTexCoord2f(0, 0);
  glVertex3f(-0.5, -0.5, -0.5);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(-0.5, 0.5, -0.5);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(-0.5, 0.5, 0.5);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(-0.5, -0.5, 0.5);

  // right
  glNormal3f(1, 0, 0);
  glTexCoord2f(0, 0);
  glVertex3f(0.5, -0.5, -0.5);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(0.5, -0.5, 0.5);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(0.5, 0.5, 0.5);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(0.5, 0.5, -0.5);

  glEnd();
}

void draw_axes()
{
  glDisable(GL_LIGHTING);
  glBegin(GL_LINES);

  glColor3f(1,0,0);
  glVertex3f(0,0,0);
  glVertex3f(9000,0,0);

  glColor3f(0,1,0);
  glVertex3f(0,0,0);
  glVertex3f(0,9000,0);

  glColor3f(0,0,1);
  glVertex3f(0,0,0);
  glVertex3f(0,0,9000);

  glEnd();
  glEnable(GL_LIGHTING);
}

void draw_floor()
{
  extern TextureManager textureManager;
  glEnable(GL_TEXTURE_2D);
  textureManager.use("floor_grid");
  glBegin(GL_QUADS);

  const float tex_repeat = 100.0;
  const float size = 100.0;
  glNormal3f(0, 1, 0);
  glTexCoord2f(0, 0);
  glVertex3f(-size, 0, -size);
  glTexCoord2f(tex_repeat, 0);
  glVertex3f(size, 0, -size);
  glTexCoord2f(tex_repeat, tex_repeat);
  glVertex3f(size, 0, size);
  glTexCoord2f(0, tex_repeat);
  glVertex3f(-size, 0, size);
  glEnd();
  glDisable(GL_TEXTURE_2D);
}
