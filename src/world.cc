#include "world.h"
#include "draw.h"

World::World()
  : _ship (new Ship),
    _cubefield(new Cubefield)
{}

World::~World()
{
  delete _ship;
  delete _cubefield;
}

void World::update(unsigned int etime)
{
  _ship->update(etime);
  _cubefield->update(_ship->pos());
}

void World::draw() const
{
  glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

  draw_floor();
  _cubefield->draw();
  _ship->draw();

  SDL_GL_SwapBuffers();
}
