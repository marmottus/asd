#ifndef EVENT_LISTENER_H_
# define EVENT_LISTENER_H_

namespace event
{
  class Event;

  class IEventListener
  {
    public:
      virtual ~IEventListener() {};
      virtual void notify(Event* event) = 0;
  };
}

#endif /* !EVENT_LISTENER_H_ */
