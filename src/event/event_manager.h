#ifndef EVENT_MANAGER_H_
# define EVENT_MANAGER_H_

# include <map>
# include <string>
# include <queue>

# include <event/events/event.h>
# include <event/event_listener.h>

namespace event
{
  class EventManager
  {
    public:
      EventManager();
      virtual ~EventManager();

      typedef std::multimap<std::string, IEventListener*>::const_iterator const_map_iterator;
      typedef std::multimap<std::string, IEventListener*>::iterator map_iterator;

      // Associate a listener to an event
      virtual void register_listener(IEventListener* listener, const std::string& event_type);
      // Do not associate anymore the given listener to an event
      virtual void unregister_listener(IEventListener* listener, const std::string& event_type);
      // Push an event to be processed later
      virtual void push_event(Event* event);
      // Process an event immediatly (without pushing it to the queue)
      virtual void fetch_event(Event* event);
      // Process all events in the queue
      virtual void fetch_events();

    protected:
      std::multimap<std::string, IEventListener*> _notify_map;
      std::queue<Event*> _event_queue;
  };
}

#endif /* !EVENT_MANAGER_H_ */
