#ifndef EVENT_H_
# define EVENT_H_

# include <string>

class Cubefield;

namespace event
{
  class IEventListener;

  class Event
  {
    public:
      Event(std::string type);
      virtual ~Event();

      virtual void accept(IEventListener* listener) = 0;
      virtual const std::string& get_type() const;

    protected:
      const std::string _type;
  };
}

# include "event.hxx"

#endif /* !EVENT_H_ */
