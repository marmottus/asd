#include <event/event_manager.h>

namespace event
{
  EventManager::EventManager ()
    : _notify_map (std::multimap<std::string, IEventListener*>()),
      _event_queue (std::queue<Event*>())
  {
  }

  EventManager::~EventManager ()
  {
  }

  void
  EventManager::register_listener (IEventListener* listener,
				   const std::string&    event_type)
  {
    _notify_map.insert(std::pair<std::string, IEventListener*>(event_type, listener));
  }

  void
  EventManager::unregister_listener (IEventListener*       listener,
                                     const std::string&    event_type)
  {
    map_iterator it = _notify_map.find(event_type);

    for (; it != _notify_map.end() && it->first == event_type; ++it)
    {
      if (it->second == listener)
      {
	_notify_map.erase(it);
	break;
      }
    }
  }

  void
  EventManager::push_event (Event* event)
  {
      _event_queue.push(event);
  }

  void
  EventManager::fetch_event (Event* event)
  {
    std::pair<const_map_iterator, const_map_iterator> it = _notify_map.equal_range(event->get_type());
    const_map_iterator elts_it = it.first;

    for (; elts_it != it.second; ++elts_it)
    {
      elts_it->second->notify(event);
    }
  }

  void
  EventManager::fetch_events ()
  {
    Event* event = 0;

    while (!_event_queue.empty())
    {
      event = _event_queue.front();
      fetch_event(event);
      delete event;
      _event_queue.pop();
    }
  }
}
