inline void
Point::set_x (const float x)
{
  _x = x;
}

inline float
Point::x () const
{
  return _x;
}

inline void
Point::set_y (const float y)
{
  _y = y;
}

inline float
Point::y () const
{
  return _y;
}

inline void
Point::set_z (const float z)
{
  _z = z;
}

inline float
Point::z () const
{
  return _z;
}

inline Point
Ship::pos () const
{
  return _mesh->center();
}

inline Point
Ship::speed () const
{
  return _speed;
}

inline Point
Chunk::center () const
{
  return _center;
}

inline void
Chunk::setVisible(bool visible)
{
  _visible = visible;
}
