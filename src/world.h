#ifndef WORLD_H
#define WORLD_H

#include "cubefield.h"

class World
{
  public:
    World();
    virtual ~World();
    void draw() const;
    void update(unsigned int etime);

    Ship* ship();

  private:
    World& operator=(const World& world) = delete;
    World(const World& world) = delete;

    Ship*      _ship;
    Cubefield* _cubefield;
};

#include "world.hxx"

#endif
