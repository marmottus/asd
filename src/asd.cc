#include <cmath>
#include <iostream>
#include <ctime>
#include <map>
#include <string>

#ifndef __APPLE__
# include <GL/gl.h>
# include <GL/glu.h>
#else
# include <OpenGL/gl.h>
# include <OpenGL/glu.h>
#endif

#include <SDL/SDL.h>

#include "cubefield.h"
#include "textureManager.h"
#include "camera.h"
#include "world.h"
#ifndef __APPLE__
# include "soundManager.h"
#endif
#include <event/event_manager.h>
#include <key/keymap.h>

// Globals
TextureManager textureManager;
struct opt
{
  int width;
  int height;
  bool fsaa;
  bool wire;
} gfx_opt;
event::EventManager g_eventManager;
#ifndef __APPLE__
SoundManager soundManager;
#endif

std::list<int> g_used_key = std::list<int>();
unsigned int   g_etime; // haters gonna hate
bool           g_done;  // also temporary
key::KeyMap    g_keymap;

void gl_init()
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0 )
  {
    std::cerr << "Unable to initialize SDL: " << SDL_GetError() << std::endl;
    exit(1);
  }
  const SDL_VideoInfo *info = SDL_GetVideoInfo();
  int flags = SDL_OPENGL;
  if (gfx_opt.fsaa)
  {
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
  }
  if (gfx_opt.width == 0) // Auto-detect
  {
    SDL_ShowCursor(SDL_DISABLE);
    gfx_opt.width = info->current_w;
    gfx_opt.height = info->current_h;
    flags |= SDL_FULLSCREEN;
  }
  if (SDL_SetVideoMode(gfx_opt.width, gfx_opt.height, 0, flags) == NULL)
  {
    std::cerr << "Unable to create OpenGL screen: " << SDL_GetError() << std::endl;
    std::cerr << "Try --no_fsaa" << std::endl;
    SDL_Quit();
    exit(2);
  }

  glViewport(0, 0, gfx_opt.width, gfx_opt.height);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-1, 1, // left, right
      -1, 1, // bottom, top
      1, 1000); // near, far

  glEnable(GL_DEPTH_TEST);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  if (gfx_opt.fsaa)
    glEnable(GL_MULTISAMPLE);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glFrontFace(GL_CW);

  // Lights
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  float light_ambient[4]  = {0.1f, 0.1f, 0.1f, 1.0f},
        light_diffuse[4]  = {0.5f, 0.5f, 0.5f, 1.0f},
        light_specular[4] = {1.0f, 1.0f, 1.0f, 1.0f},
        light_pos[4] = {1.0f, 1.0f, 1.0f};
  glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
  glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

  // Textures
  #define TEX_SIZE 256
  GLubyte img[TEX_SIZE][TEX_SIZE][4];
  int i, j, c;
  for (i = 0; i < TEX_SIZE; i++)
    for (j = 0; j < TEX_SIZE; j++)
    {
      // Create grid texture
      c = (!i || !j)*255;
      img[i][j][0] = (GLubyte) c;
      img[i][j][1] = (GLubyte) c;
      img[i][j][2] = (GLubyte) c;
      img[i][j][3] = (GLubyte) 255;
    }
  textureManager.add("floor_grid", img, TEX_SIZE);

  // Fog
  GLfloat density = 0.03;
  GLfloat fogColor[4] = {0.0, 0.0, 0.0, 1.0};
  glEnable (GL_FOG);
  glFogi (GL_FOG_MODE, GL_EXP);
  glFogfv (GL_FOG_COLOR, fogColor);
  glFogf (GL_FOG_DENSITY, density);
  glHint (GL_FOG_HINT, GL_NICEST);
}

#define PRINT(value) \
  (std::cout << #value  \
   << (value ? "\t\033[0;32m[OK]\033[00m" : "\t\033[0;31m[KO]\033[00m") \
   << std::endl)

static void gfx_test()
{
  bool SDL     = false;
  bool FSAA    = false;

  // SDL
  SDL = SDL_Init(SDL_INIT_VIDEO) >= 0;

  // FSAA
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
  FSAA = SDL_SetVideoMode(10, 10, 0, SDL_OPENGL) != NULL;

  PRINT(SDL);
  PRINT(FSAA);
  exit(0);
}

void manage_options(int argc, char **argv)
{
  const std::string usage =
    "Usage: ./asd [OPTIONS]\n"
    "\t--small        run in a 800x600 window\n"
    "\t--no_fsaa      turn off fullscreen anti-aliasing\n"
    "\t--gfx_test     test your system to see what feature you can use";

  for (int i = 1; i < argc; i++)
  {
    if (std::string(argv[i]) == "--help")
    {
      std::cout << usage << std::endl;
      exit(0);
    }
    if (std::string(argv[i]) == "--gfx_test")
      gfx_test();
    else if (std::string(argv[i]) == "--small")
    {
      gfx_opt.width = 800;
      gfx_opt.height = 600;
    }
    else if (std::string(argv[i]) == "--no_fsaa")
      gfx_opt.fsaa = false;
    else
    {
      std::cerr << "Wrong argument: " << argv[i] << std::endl;
      std::cout << usage << std::endl;
      exit(1);
    }
  }
}

namespace
{
  void ship_add_z(key::handlerParams* params)
  {
    params->world.ship()->addSpeed(0.0, 0.0, g_etime/1000.0*SHIP_ACCELERATION);
  }

  void ship_sub_z(key::handlerParams* params)
  {
    params->world.ship()->addSpeed(0.0, 0.0, -(g_etime/1000.0)*SHIP_ACCELERATION);
  }

  void ship_add_x(key::handlerParams* params)
  {
    params->world.ship()->addSpeed(g_etime/1000.0*SHIP_ACCELERATION, 0.0, 0.0);
  }

  void ship_sub_x(key::handlerParams* params)
  {
    params->world.ship()->addSpeed(-(g_etime/1000.0)*SHIP_ACCELERATION, 0.0, 0.0);
  }

  void ship_add_y(key::handlerParams* params)
  {
    params->world.ship()->addSpeed(0.0, g_etime/1000.0*SHIP_ACCELERATION, 0.0);
  }

  void ship_sub_y(key::handlerParams* params)
  {
    params->world.ship()->addSpeed(0.0, -(g_etime/1000.0)*SHIP_ACCELERATION, 0.0);
  }

  void cam_add_x(key::handlerParams* params)
  {
    params->cam.set_x(params->cam.x() + 0.1);
  }

  void cam_sub_x(key::handlerParams* params)
  {
    params->cam.set_x(params->cam.x() - 0.1);
  }

  void cam_add_y(key::handlerParams* params)
  {
    params->cam.set_y(params->cam.y() + 0.1);
  }

  void cam_sub_y(key::handlerParams* params)
  {
    params->cam.set_y(params->cam.y() - 0.1);
  }

  void cam_add_z(key::handlerParams* params)
  {
    params->cam.set_z(params->cam.z() + 0.1);
  }

  void cam_sub_z(key::handlerParams* params)
  {
    params->cam.set_z(params->cam.z() - 0.1);
  }

  void set_exit_flag(key::handlerParams* params)
  {
    params = params; // removes warning
    g_done = true;
  }

  void cam_reset(key::handlerParams* params)
  {
    params->cam.set_pos(Point(-10, 8, 0));
  }

  void set_wire_mode(key::handlerParams* params)
  {
    (void) params;
    if (params->state == key::RELEASED)
    {
      gfx_opt.wire = !gfx_opt.wire;
      glPolygonMode(GL_FRONT_AND_BACK, gfx_opt.wire ? GL_LINE : GL_FILL);
    }
  }

  void set_view_follow(key::handlerParams* params)
  {
    (void) params;
    params->cam.changeView(FOLLOW);
  }

  void set_view_up(key::handlerParams* params)
  {
    (void) params;
    params->cam.changeView(UP);
  }
}

void manage_keys(SDL_Event &event, World& world, Camera &cam)
{
  // Manage events
  SDL_PollEvent(&event);

  Uint8* key_array = SDL_GetKeyState(NULL);

  for (auto &key : g_used_key)
  {
    if (key_array[key])
      g_keymap.handle_key(key, world, cam);
    else
      g_keymap.release_key(key);
  }
}

void init_keys()
{
  // Set keys to be used
  g_used_key.push_front(SDLK_RIGHT);
  g_used_key.push_front(SDLK_LEFT);
  g_used_key.push_front(SDLK_UP);
  g_used_key.push_front(SDLK_DOWN);
  g_used_key.push_front(SDLK_SPACE);
  g_used_key.push_front(SDLK_c);
  g_used_key.push_front(SDLK_u);
  g_used_key.push_front(SDLK_o);
  g_used_key.push_front(SDLK_k);
  g_used_key.push_front(SDLK_i);
  g_used_key.push_front(SDLK_j);
  g_used_key.push_front(SDLK_l);
  g_used_key.push_front(SDLK_r);
  g_used_key.push_front(SDLK_q);
  g_used_key.push_front(SDLK_w);
  g_used_key.push_front(SDLK_F1);
  g_used_key.push_front(SDLK_F2);

  // Set default key mapping
  g_keymap.map_key(SDLK_RIGHT, ship_add_z);
  g_keymap.map_key(SDLK_LEFT, ship_sub_z);
  g_keymap.map_key(SDLK_UP, ship_add_x);
  g_keymap.map_key(SDLK_DOWN, ship_sub_x);
  g_keymap.map_key(SDLK_SPACE, ship_add_y);
  g_keymap.map_key(SDLK_c, ship_sub_y);
  g_keymap.map_key(SDLK_u, cam_add_x);
  g_keymap.map_key(SDLK_o, cam_sub_x);
  g_keymap.map_key(SDLK_k, cam_add_y);
  g_keymap.map_key(SDLK_i, cam_sub_y);
  g_keymap.map_key(SDLK_j, cam_add_z);
  g_keymap.map_key(SDLK_l, cam_sub_z);
  g_keymap.map_key(SDLK_r, cam_reset);
  g_keymap.map_key(SDLK_q, set_exit_flag);
  g_keymap.map_key(SDLK_w, set_wire_mode);
  g_keymap.map_key(SDLK_F1, set_view_follow);
  g_keymap.map_key(SDLK_F2, set_view_up);
}

int main(int argc, char **argv)
{
  // Init gfx_opt with default values
  gfx_opt.width = 0; // autodetect
  gfx_opt.height = 0; // autodetect
  gfx_opt.fsaa = true;
  gfx_opt.wire = false;
  // Fill gfx_opt
  manage_options(argc, argv);


  srand(time(NULL));
  gl_init(); // Initialize SDL and OpenGL
  SDL_Event event;
  /*
   * Time variables
   * g_etime: ellapsed time (time between two frames)
   */
  unsigned int last_time, fps = 0,
               last_frame = SDL_GetTicks(), time = SDL_GetTicks();

  Camera camera(Point(-10, 8, 0), Point(0, 0, 0));
  World world;

  init_keys();

  #ifndef __APPLE__
    // Sound Initialization
    soundManager.addSound("lol", "media/sound/pan.mp3");
  #endif

  while (!g_done)
  {
    // Update time variables
    last_time = time;
    time = SDL_GetTicks();
    g_etime = time - last_time;

    // Free the CPU
    if (g_etime < 15)
      SDL_Delay(15 - g_etime);

    // Print FPS count
    fps++;
    if (time - last_frame >= 1000)
    {
      std::cout << "fps: " << fps << std::endl;
      fps = 0;
      last_frame = time;
    }

    camera.update(world.ship());

    world.draw(); // Draw all the things!
    manage_keys(event, world, camera); // Keyboard inputs
    world.update(g_etime); // Update objects' positions
    g_eventManager.fetch_events();
  }

  SDL_Quit();
  return 0;
}
