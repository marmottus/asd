#include "keymap.h"

namespace key
{
  KeyMap::KeyMap ()
    : _key_map (key_map())
  {
  }

  KeyMap::KeyMap (key_map& map)
    : _key_map (map)
  {
  }

  KeyMap::~KeyMap ()
  {
    key_map::iterator it = _key_map.begin();

    for (; it != _key_map.end(); ++it)
      delete it->second;
  }

  void
  KeyMap::map_key (key_type         key,
                   const keyHandler handler)
  {
    KeyData* data = new KeyData;

    data->lastState = RELEASED;
    data->handler = handler;
    _key_map[key] = data;
  }
}
