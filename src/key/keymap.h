#ifndef KEYMAP_H_
# define KEYMAP_H_

# include <map>

class World;
class Camera;

namespace key
{
  enum keyState
  {
    PRESSED,
    RELEASED
  };

  typedef struct handlerParams
  {
      World&     world;
      Camera&    cam;
      keyState   state;
  } handlerParams;

  typedef void (*keyHandler)(handlerParams* params);

  struct KeyData
  {
    keyState   lastState;
    keyHandler handler;
  };

  class KeyMap
  {
    public:
      typedef int key_type;
      typedef std::map<key_type, KeyData*> key_map;

      KeyMap();
      KeyMap(key_map& map);
      ~KeyMap();

      void map_key(key_type key, const keyHandler handler);
      void handle_key(key_type key, World& world, Camera& cam);
      void release_key(key_type key);

      // Getter
      const key_map& get_key_map() const;

    private:
      key_map _key_map;
  };
}

# include <key/keymap.hxx>

#endif /* !KEYMAP_H_ */
