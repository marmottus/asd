namespace key
{
  const inline KeyMap::key_map&
  KeyMap::get_key_map () const
  {
    return _key_map;
  }

  inline void
  KeyMap::handle_key (key_type   key,
                      World& world,
		      Camera&    cam)
  {
    handlerParams params{world, cam, _key_map[key]->lastState};

    _key_map[key]->handler(&params);
    _key_map[key]->lastState = PRESSED;
  }

  inline void
  KeyMap::release_key (KeyMap::key_type key)
  {
    _key_map[key]->lastState = RELEASED;
  }
}
