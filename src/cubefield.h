#ifndef CUBEFIELD_H
#define CUBEFIELD_H

#include <list>
#include <SDL/SDL.h>

#ifndef __APPLE__
 #include <GL/gl.h>
#else
 #include <OpenGL/gl.h>
#endif

#define NB_CUBES 5
// base space between two cubes
#define SPACING  3.0
// the maximum distance a cube can
// vary from its base position
#define MAX_DIFF 5
#define CUBE_SIZE 1.0

#define MAX_SHIP_SPEED 0.02
#define SHIP_ACCELERATION 0.05
// Makes the ship slowly decelerate
#define SHIP_FRICTION 0.01

class Point
{
  public:
    Point(float x = 0, float y = 0, float z = 0);
    // Add a random displacement (only in x and z)
    void addRandomDisplacement();
    void translate() const; // does a glTranslate()

    float x() const;
    float y() const;
    float z() const;

    void set_x(const float x);
    void set_y(const float y);
    void set_z(const float z);

  private:
    float _x, _y, _z;
};

class Color
{
  public:
    // Create a random color
    Color();
    Color(float r, float g, float b);
    void use() const; // Use this color (glColor3f)
    static void use(float r, float g, float b);
    float r() const { return _r; }
    float g() const { return _g; }
    float b() const { return _b; }

  private:
    float _r, _g, _b;
};

class Cube
{
  public:
    Cube(const Point &center);
    void draw() const;
    void translate() const;
    float size()   const{ return _size; }
    float angle()   const{ return _angle; }
    Point center() const { return _center; }
    Color color()  const { return _color;}
    void  set_color(Color c){_color = c;}
    void  set_color(float r, float g, float b);
    void  set_center(float x, float y, float z);

  private:
    float _size;
    float _angle;
    Point _center;
    Color _color;
};

// Move to another file
// TODO : Add speed values
class Ship
{
  public:
    Ship();
    ~Ship();
    void draw() const;
    void update(unsigned int etime);
    void addSpeed(float x, float y, float z);
    Point pos() const;
    Point speed() const;
  private:
    Ship(const Ship& ship) = delete;
    Ship& operator=(const Ship& ship) = delete;

    Cube* _mesh;
    Point _speed;
};

class Chunk
{
  public:
    Chunk(const Point &center);
    virtual ~Chunk();
    void draw() const;
    Point center() const;
    void setVisible(bool visible);

  private:
    Chunk(const Chunk& chunk) = delete;
    Chunk& operator=(const Chunk& chunk) = delete;

    Point _center;
    std::list<Cube> _cubes;
    GLuint _dl; // display list
    bool _visible;
};

class Cubefield
{
  public:
    Cubefield();
    virtual ~Cubefield();
    void draw() const;
    void update(const Point &ship_pos);

  private:
    Cubefield(const Cubefield& cubefield) = delete;
    Cubefield& operator=(const Cubefield& cubefield) = delete;

    std::list<Chunk*> _chunks;
};

#include "cubefield.hxx"

#endif
