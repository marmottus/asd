#include "soundManager.h"
#include <stdexcept>
#include <iostream>

SoundManager::SoundManager()
    : _map (std::map<std::string, FMOD_SOUND*>()),
      _system (0)
{
  if (FMOD_System_Create(&_system) != FMOD_OK
      || FMOD_System_Init(_system, 2, FMOD_INIT_NORMAL, NULL) != FMOD_OK)
    std::cerr << "Could not initialize FMOD." << std::endl;
}

SoundManager::~SoundManager()
{
  std::map<std::string, FMOD_SOUND*>::iterator it;
  for (it = _map.begin(); it != _map.end(); it++)
    FMOD_Sound_Release(it->second);
  _map.clear();
  FMOD_System_Close(_system);
  FMOD_System_Release(_system);
}

void SoundManager::addSound(const std::string &name,
                            const std::string &path)
{
  if (_map.find(name) != _map.end())
  {
    std::string msg = "Sound or music already exists: " + name;
    throw std::runtime_error(msg);
  }
  _map[name] = NULL;
  if (FMOD_System_CreateSound(_system,
                            path.c_str(),
                            FMOD_CREATESAMPLE,
                            0,
                            &_map[name]) != FMOD_OK)
  {
    std::string msg = "Unable to read file: " + path;
    throw std::runtime_error(msg);
  }
}

void SoundManager::addMusic(const std::string &name,
                            const std::string &path)
{
  if (_map.find(name) != _map.end())
  {
    std::string msg = "Sound or music already exists: " + name;
    throw std::runtime_error(msg);
  }
  _map[name] = NULL;
  if (FMOD_System_CreateSound(_system,
                            path.c_str(),
                            FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM,
                            0,
                            &_map[name]) != FMOD_OK)
  {
    std::string msg = "Unable to read file: " + path;
    throw std::runtime_error(msg);
  }
}

void SoundManager::play(const std::string &name)
{
  if (_map.find(name) == _map.end())
  {
    std::string msg = "Could not find sound: " + name;
    throw std::runtime_error(msg);
  }
  FMOD_System_PlaySound(_system, FMOD_CHANNEL_FREE, _map[name], 0, NULL);
}
