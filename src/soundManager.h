#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include <map>
#include <string>
#include <fmodex/fmod.h>

class SoundManager
{
  public:
    SoundManager();
    ~SoundManager();
    void addSound(const std::string &name,
                  const std::string &path);
    void addMusic(const std::string &name,
                  const std::string &path);
    void play(const std::string &name);

  private:
    SoundManager(const SoundManager& soundManager) = delete;
    SoundManager& operator=(const SoundManager& soundManager) = delete;

    std::map<std::string, FMOD_SOUND*> _map;
    FMOD_SYSTEM *_system;
};

#endif
