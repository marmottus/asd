#ifndef CAMERA_H
#define CAMERA_H

#include "cubefield.h"

#define ROLL_COEF 200.0

enum camView{FOLLOW, UP};

class Camera
{
  public:
    Camera(const Point &pos, const Point &target);
    void set_pos(const Point &pos);
    void set_x(const float x);
    void set_y(const float y);
    void set_z(const float z);
    void set_roll(const float roll);
    float x() const;
    float y() const;
    float z() const;

    void set_target(const Point &target);
    Point pos() const;
    Point target() const;
    void update(const Ship *ship);
    void changeView(camView view);

  private:
    Point   _pos;
    Point   _target;
    float   _roll;
    camView _view;
};

#include "camera.hxx"

#endif
