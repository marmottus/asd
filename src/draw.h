/*
 * draw.h
 *
 * Functions to draw common objects.
 * All the functions assume the 'up' vector is (0, 1, 0)
 */

#ifndef DRAW_H
#define DRAW_H

/*
 * Draw a cube centered at the origin.
 * Size: 2x2x2
 */
void draw_cube();

/*
 * Debug function showing the axis
 * X: red
 * Y: green
 * Z: blue
 */
void draw_axes();

/*
 * Draw a very big plane with y=0
 */
void draw_floor();

#endif
