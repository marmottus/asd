#include "textureManager.h"
#ifndef __APPLE__
 #include <GL/glu.h>
#else
 #include <OpenGL/glu.h>
#endif
#include <stdexcept>

TextureManager::TextureManager()
  : _map (std::map<std::string, GLuint>())
{
}

TextureManager::~TextureManager()
{
  _map.clear();
}

void TextureManager::add(const std::string &name,
                         const void *data,
                         GLsizei size)
{
  GLuint texId;
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glGenTextures(1, &texId);
  glBindTexture(GL_TEXTURE_2D, texId);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  gluBuild2DMipmaps(GL_TEXTURE_2D, 3, size, size, GL_RGBA, GL_UNSIGNED_BYTE, data);
  _map[name] = texId;
}

void TextureManager::use(const std::string &name)
{
  if (_map.find(name) == _map.end())
  {
    std::string msg = "Could not find texture: " + name;
    throw std::runtime_error(msg);
  }
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  glBindTexture(GL_TEXTURE_2D, _map[name]);
}
