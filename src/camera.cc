#ifndef __APPLE__
 #include <GL/glu.h>
#else
 #include <OpenGL/glu.h>
#endif
#include "camera.h"
#include <cmath>

Camera::Camera(const Point &pos, const Point &target)
  : _pos(pos), _target(target), _roll(0), _view(FOLLOW)
{}

void Camera::changeView(camView view)
{
  _view = view;
}

void Camera::update(const Ship *ship)
{
  switch (_view)
  {
    case FOLLOW:
      // Always look at the ship
      set_target(ship->pos());
      // Position the camera juste above and behind the ship
      set_pos(ship->pos());
      set_y(y() + 5.0);
      set_x(x() - 7.0);
      set_roll(ship->speed().z() * ROLL_COEF);
      break;

    case UP:
      set_target(Point(0.001,0,0));
      set_y(20);
      set_x(0);
      set_z(0);
      set_roll(0);
      break;
  }


  // Update camera position
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glRotatef(_roll, 0, 0, 1);
  gluLookAt(_pos.x(), _pos.y(), _pos.z(),
            _target.x(), _target.y(), _target.z(),
            0, 1, 0);
}
