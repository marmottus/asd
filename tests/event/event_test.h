#ifndef TEST_H_
# define TEST_H_

# include <event/events/event.h>
# include <event/event_listener.h>

class MyEvent : public event::Event
{
  public:
    typedef event::Event super;
    MyEvent(std::string msg);
    std::string _my_msg;
};

class MyListener : public event::IEventListener
{
  public:
    MyListener();
    virtual bool notify (event::Event* event);
};

class MyListener2 : public event::IEventListener
{
  public:
    MyListener2();
    virtual bool notify (event::Event* event);
};

#endif /* !TEST_H_ */
