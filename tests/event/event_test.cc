#include <event/event_manager.h>
#include "event_test.h"
#include <iostream>
#include <assert.h>

MyEvent::MyEvent (std::string msg)
  : super(std::string("moemoe")),
    _my_msg (msg)
{
}

MyListener::MyListener ()
{
}

bool
MyListener::notify (event::Event* event)
{
  if (event->get_type() == "moemoe")
  {
    std::string msg = (static_cast<MyEvent*> (event))->_my_msg;
    if (msg == "moemoekyun")
    {
      std::cout << "Basic event notify 1 : \033[32mOK\033[0m" << std::endl;
      return true;
    }
  }
  std::cout << "Basic event notify 1 : \033[31mKO\033[0m" << std::endl;
  return false;
}

MyListener2::MyListener2 ()
{
}

bool
MyListener2::notify (event::Event* event)
{
  if (event->get_type() == "moemoe")
  {
    std::string msg = (static_cast<MyEvent*> (event))->_my_msg;
    if (msg == "moemoekyun")
    {
      std::cout << "Basic event notify 2 : \033[32mOK\033[0m" << std::endl;
      return true;
    }
  }
  std::cout << "Basic event notify 2 : \033[31mKO\033[0m" << std::endl;
  return false;
}

bool register_listener_test()
{
  event::EventManager* eManager = new event::EventManager;
  MyListener* listener = new MyListener;
  MyListener2* listener2 = new MyListener2;
  MyEvent event = MyEvent(std::string("moemoekyun"));

  eManager->register_listener(listener, event.get_type());
  eManager->register_listener(listener2, event.get_type());
  eManager->push_event(&event);
  eManager->fetch_events();
  eManager->fetch_events();

  delete listener2;
  delete listener;
  delete eManager;
}

int main(int  argc,
         char **argv)
{
  std::cout << "Event test suite" << std::endl;
  register_listener_test();

  return 0;
}
