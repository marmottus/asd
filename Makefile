SOURCES         = src/asd.cc                    \
                 src/draw.cc                   \
                 src/cubefield.cc              \
                 src/world.cc                  \
                 src/camera.cc                 \
                 src/textureManager.cc         \
                 src/event/events/event.cc     \
                 src/key/keymap.cc             \
                 src/event/event_manager.cc

_OBJS            = $(SOURCES:src/%.cc=$(OBJS_DIR)/%.o)
OBJS_DIR        = .objs

LDFLAGS         ?= -lSDL
CPPFLAGS        ?= -O2 -Wall -Wextra -pedantic -Weffc++ -std=c++0x -Isrc -I.
CXXFLAGS        ?= -L. -Lsrc
CXX             ?= g++

BIN             = asd

ifeq ($(shell uname), Linux)
SOURCES         += src/soundManager.cc
LDFLAGS         += -lm -lGL -lGLU -lfmodex
CPPFLAGS        += -I/usr/include/SDL -I/usr/include/fmodex
endif

ifeq ($(shell uname), Darwin)
LDFLAGS         += -lSDLmain -framework Cocoa -framework OpenGL
endif

# Test the existence of .deps
$(shell                                                                 \
if ! [ -f .deps ]; then                                                 \
  $(RM) .Makefile.dep &&                                                \
  for f in $(SOURCES); do                                               \
    obj=$(OBJS_DIR)/$${f#src/};                                         \
     $(CXX) $(CPPFLAGS) -MT$${obj%.*}.o -MM $$f>> .Makefile.dep;        \
  done;                                                                 \
fi)

_CLEAN        = echo "test -z \"$$f\" || ! test -f \"$$f\" || $(RM) $$f";     \
                test -z "$$f" || ! test -f "$$f" || $(RM) $$f
_CLEAN_R      = echo "test -z \"$$f\" || ! test -r \"$$f\" || $(RM) -r $$f";  \
                test -z "$$f" || ! test -r "$$f" || $(RM) -r $$f

all: $(BIN)

$(BIN): $(_OBJS)
	$(CXX_MSG)
	$(CXX) $(CXXFLAGS) $^ $(LDFLAGS)    -o $@

.objs/%.o: src/%.cc
	@test -d `dirname $@` || mkdir -p `dirname $@`
	$(CPP_MSG)
	$(CXX) -c $(CPPFLAGS) -o $@     $<

include .Makefile.dep

clean:
	@for f in $(_OBJS) $(BIN); do   \
	  $(_CLEAN);                    \
	done
	@f=$(OBJS_DIR);                 \
	$(_CLEAN_R)


distclean: clean
	@for f in .Makefile.dep; do         \
	  $(_CLEAN);                        \
	done
